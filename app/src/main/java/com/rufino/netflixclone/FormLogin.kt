package com.rufino.netflixclone

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.InputBinding
import android.widget.Toast
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.rufino.netflixclone.databinding.ActivityFormLoginBinding

class FormLogin : AppCompatActivity() {

    private lateinit var binding: ActivityFormLoginBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityFormLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        supportActionBar!!.hide()
        VerificarUsuarioLogado()
        binding.btEntrar.setOnClickListener {
            val email = binding.editEmail.text.toString()
            val senha = binding.editSenha.text.toString()
            val mensagemErro = binding.mensagemErro

            if(email.isEmpty() || senha.isEmpty()){
                mensagemErro.setText("Preencha todos os campos!")
            }else{
                AutenticarUsuario()
            }
        }

        binding.linkCadastrar.setOnClickListener {
            val intent = Intent(this, FormCadastro::class.java)
            startActivity(intent)
        }
    }

    private fun AutenticarUsuario(){

        val email = binding.editEmail.text.toString()
        val senha = binding.editSenha.text.toString()
        val mensagemErro = binding.mensagemErro

        FirebaseAuth.getInstance().signInWithEmailAndPassword(email, senha).addOnCompleteListener{
            if(it.isSuccessful){
                Toast.makeText(this,"Login efetuado com sucesso!",Toast.LENGTH_SHORT).show()
                binding.editEmail.setText("")
                binding.editSenha.setText("")
                IrParaListaFilmes()
            }
        }.addOnFailureListener{
            var erro = it
            when {
                erro is FirebaseAuthInvalidCredentialsException -> mensagemErro.setText("E-mail ou senha inválidos.")
                erro is FirebaseNetworkException -> mensagemErro.setText("Sem conexão com a internet.")
                else -> mensagemErro.setText("Erro ao logar!")
            }
        }
    }

    private fun VerificarUsuarioLogado(){
        val usuarioLogado = FirebaseAuth.getInstance().currentUser

        if(usuarioLogado != null)
            IrParaListaFilmes()
    }

    private fun IrParaListaFilmes(){
        val intent = Intent(this, ListaFilmes::class.java)
        startActivity(intent)
    }
}